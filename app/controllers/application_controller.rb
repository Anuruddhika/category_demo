class ApplicationController < ActionController::Base
  before_action :category

  def category
    @categories = Category.roots
  end
end
