class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.references :category, foreign_key: true
      t.string :name
      t.string :price
      t.string :quantity

      t.timestamps
    end
  end
end
